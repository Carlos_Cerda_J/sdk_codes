/*
 * File Name:         hdl_prj\ipcore\NLC_2cell_dual_M2C_v1_2\include\NLC_2cell_dual_M2C_addr.h
 * Description:       C Header File
 * Created:           2021-01-27 17:23:27
*/

#ifndef NLC_2CELL_DUAL_M2C_H_
#define NLC_2CELL_DUAL_M2C_H_

#define  IPCore_Reset_NLC_2cell_dual_M2C        0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_NLC_2cell_dual_M2C       0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_NLC_2cell_dual_M2C    0x8  //contains unique IP timestamp (yymmddHHMM): 2101271723
#define  Vdc_inv_Data_NLC_2cell_dual_M2C        0x100  //data register for Inport Vdc_inv
#define  Vdc_inv_prop_Data_NLC_2cell_dual_M2C   0x104  //data register for Inport Vdc_inv_prop
#define  selector_NLC_Data_NLC_2cell_dual_M2C   0x108  //data register for Inport selector_NLC
#define  selector_V0S_Data_NLC_2cell_dual_M2C   0x10C  //data register for Inport selector_V0S
#define  Vap_ref_Data_NLC_2cell_dual_M2C        0x110  //data register for Inport Vap_ref
#define  Vbp_ref_Data_NLC_2cell_dual_M2C        0x114  //data register for Inport Vbp_ref
#define  Vcp_ref_Data_NLC_2cell_dual_M2C        0x118  //data register for Inport Vcp_ref
#define  Van_ref_Data_NLC_2cell_dual_M2C        0x11C  //data register for Inport Van_ref
#define  Vbn_ref_Data_NLC_2cell_dual_M2C        0x120  //data register for Inport Vbn_ref
#define  Vcn_ref_Data_NLC_2cell_dual_M2C        0x124  //data register for Inport Vcn_ref

#endif /* NLC_2CELL_DUAL_M2C_H_ */
