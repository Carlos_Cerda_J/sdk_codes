/*
 * File Name:         hdl_prj\ipcore\PR_oversampF2_v1_0\include\PR_oversampF2_addr.h
 * Description:       C Header File
 * Created:           2019-03-26 13:18:38
*/

#ifndef PR_OVERSAMPF2_H_
#define PR_OVERSAMPF2_H_

#define  IPCore_Reset_PR_oversampF2       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PR_oversampF2      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PR_oversampF2   0x8  //contains unique IP timestamp (yymmddHHMM): 1903261318
#define  kp_Data_PR_oversampF2            0x100  //data register for Inport kp
#define  umax_pos_Data_PR_oversampF2      0x108  //data register for Inport umax_pos
#define  b0_Data_PR_oversampF2            0x10C  //data register for Inport b0
#define  a1_Data_PR_oversampF2            0x110  //data register for Inport a1
#define  umax_neg_Data_PR_oversampF2      0x118  //data register for Inport umax_neg

#endif /* PR_OVERSAMPF2_H_ */
