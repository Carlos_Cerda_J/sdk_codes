/*
 * File Name:         C:\IPCORES\3PH_SINE\ipcore\THREE_SINE_IP_v1_0\include\THREE_SINE_IP_addr.h
 * Description:       C Header File
 * Created:           2019-01-10 21:26:38
*/

#ifndef THREE_SINE_IP_H_
#define THREE_SINE_IP_H_

#define  IPCore_Reset_THREE_SINE_IP       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_THREE_SINE_IP      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_THREE_SINE_IP   0x8  //contains unique IP timestamp (yymmddHHMM): 1901102126
#define  amplitude_Data_THREE_SINE_IP     0x100  //data register for Inport amplitude
#define  Van__Data_THREE_SINE_IP          0x104  //data register for Outport Van_
#define  Vbn__Data_THREE_SINE_IP          0x108  //data register for Outport Vbn_
#define  Vcn__Data_THREE_SINE_IP          0x10C  //data register for Outport Vcn_

#endif /* THREE_SINE_IP_H_ */
