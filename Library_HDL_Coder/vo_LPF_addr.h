/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\LPF_2order\vo_lpf2\ipcore\vo_LPF_v2_0\include\vo_LPF_addr.h
 * Description:       C Header File
 * Created:           2019-09-25 17:50:18
*/

#ifndef VO_LPF_H_
#define VO_LPF_H_

#define  IPCore_Reset_vo_LPF        0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_vo_LPF       0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_vo_LPF    0x8  //contains unique IP timestamp (yymmddHHMM): 1909251750
#define  out_filtered_Data_vo_LPF   0x100  //data register for Outport out_filtered
#define  a1_Data_vo_LPF             0x104  //data register for Inport a1
#define  a2_Data_vo_LPF             0x108  //data register for Inport a2
#define  a3_Data_vo_LPF             0x10C  //data register for Inport a3
#define  a4_Data_vo_LPF             0x110  //data register for Inport a4
#define  a5_Data_vo_LPF             0x114  //data register for Inport a5
#define  offset_Data_vo_LPF         0x11C  //data register for Inport offset

#endif /* VO_LPF_H_ */
