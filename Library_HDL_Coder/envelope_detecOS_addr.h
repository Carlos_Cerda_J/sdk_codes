/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\envelope_detector\env_detector_os\ipcore\envelope_detecOS_v1_0\include\envelope_detecOS_addr.h
 * Description:       C Header File
 * Created:           2019-09-03 09:54:50
*/

#ifndef ENVELOPE_DETECOS_H_
#define ENVELOPE_DETECOS_H_

#define  IPCore_Reset_envelope_detecOS       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_envelope_detecOS      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_envelope_detecOS   0x8  //contains unique IP timestamp (yymmddHHMM): 1909030954
#define  count_max_Data_envelope_detecOS     0x100  //data register for Inport count_max
#define  step_Data_envelope_detecOS          0x104  //data register for Inport step
#define  env_out_Data_envelope_detecOS       0x108  //data register for Outport env_out

#endif /* ENVELOPE_DETECOS_H_ */
