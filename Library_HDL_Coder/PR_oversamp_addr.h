/*
 * File Name:         hdl_prj\ipcore\PR_oversamp_v1_0\include\PR_oversamp_addr.h
 * Description:       C Header File
 * Created:           2019-01-29 21:37:59
*/

#ifndef PR_OVERSAMP_H_
#define PR_OVERSAMP_H_

#define  IPCore_Reset_PR_oversamp       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PR_oversamp      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PR_oversamp   0x8  //contains unique IP timestamp (yymmddHHMM): 1901292137
#define  a1_Data_PR_oversamp            0x100  //data register for Inport a1
#define  a2_Data_PR_oversamp            0x104  //data register for Inport a2
#define  umax_pos_Data_PR_oversamp      0x108  //data register for Inport umax_pos
#define  b0_Data_PR_oversamp            0x10C  //data register for Inport b0
#define  b1_Data_PR_oversamp            0x110  //data register for Inport b1
#define  b2_Data_PR_oversamp            0x114  //data register for Inport b2
#define  umax_neg_Data_PR_oversamp      0x118  //data register for Inport umax_neg

#endif /* PR_OVERSAMP_H_ */
