/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\sort_3_cell\PULSE_DELAY_V2\ipcore\PULSE_DELAY_NLC_v2_0\include\PULSE_DELAY_NLC_addr.h
 * Description:       C Header File
 * Created:           2019-09-11 14:23:26
*/

#ifndef PULSE_DELAY_NLC_H_
#define PULSE_DELAY_NLC_H_

#define  IPCore_Reset_PULSE_DELAY_NLC       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PULSE_DELAY_NLC      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PULSE_DELAY_NLC   0x8  //contains unique IP timestamp (yymmddHHMM): 1909111423
#define  delayMAX_Data_PULSE_DELAY_NLC      0x100  //data register for Inport delayMAX

#endif /* PULSE_DELAY_NLC_H_ */
