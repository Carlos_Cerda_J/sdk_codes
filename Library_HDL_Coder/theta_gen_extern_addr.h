/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\SINE_LUT_EXTERN\theta_gen_extern\ipcore\theta_gen_extern_v1_0\include\theta_gen_extern_addr.h
 * Description:       C Header File
 * Created:           2019-07-10 15:30:59
*/

#ifndef THETA_GEN_EXTERN_H_
#define THETA_GEN_EXTERN_H_

#define  IPCore_Reset_theta_gen_extern       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_theta_gen_extern      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_theta_gen_extern   0x8  //contains unique IP timestamp (yymmddHHMM): 1907101530
#define  limit_Data_theta_gen_extern         0x100  //data register for Inport limit

#endif /* THETA_GEN_EXTERN_H_ */
