/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\sort_3_cell\CONSTANT_2514\ipcore\CONSTANT_2514_v1_0\include\CONSTANT_2514_addr.h
 * Description:       C Header File
 * Created:           2019-09-10 12:06:57
*/

#ifndef CONSTANT_2514_H_
#define CONSTANT_2514_H_

#define  IPCore_Reset_CONSTANT_2514       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_CONSTANT_2514      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_CONSTANT_2514   0x8  //contains unique IP timestamp (yymmddHHMM): 1909101206
#define  vcap4_Data_CONSTANT_2514         0x100  //data register for Inport vcap4
#define  vcap5_Data_CONSTANT_2514         0x104  //data register for Inport vcap5
#define  vcap6_Data_CONSTANT_2514         0x108  //data register for Inport vcap6
#define  vcan4_Data_CONSTANT_2514         0x10C  //data register for Inport vcan4
#define  vcan5_Data_CONSTANT_2514         0x110  //data register for Inport vcan5
#define  vcan6_Data_CONSTANT_2514         0x114  //data register for Inport vcan6
#define  vcref_Data_CONSTANT_2514         0x118  //data register for Inport vcref

#endif /* CONSTANT_2514_H_ */
