/*
 * File Name:         E:\xilinx\IPCORES\selector_2x1\ipcore\switch_dpdt_v1_0\include\switch_dpdt_addr.h
 * Description:       C Header File
 * Created:           2019-01-31 18:35:13
*/

#ifndef SWITCH_DPDT_H_
#define SWITCH_DPDT_H_

#define  IPCore_Reset_switch_dpdt       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_switch_dpdt      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_switch_dpdt   0x8  //contains unique IP timestamp (yymmddHHMM): 1901311835
#define  select_Data_switch_dpdt        0x100  //data register for Inport select

#endif /* SWITCH_DPDT_H_ */
