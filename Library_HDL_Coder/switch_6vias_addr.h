/*
 * File Name:         E:\sorting\switch_2way\ipcore\switch_6vias_v1_0\include\switch_6vias_addr.h
 * Description:       C Header File
 * Created:           2019-04-30 10:02:14
*/

#ifndef SWITCH_6VIAS_H_
#define SWITCH_6VIAS_H_

#define  IPCore_Reset_switch_6vias       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_switch_6vias      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_switch_6vias   0x8  //contains unique IP timestamp (yymmddHHMM): 1904301002
#define  select_Data_switch_6vias        0x100  //data register for Inport select

#endif /* SWITCH_6VIAS_H_ */
